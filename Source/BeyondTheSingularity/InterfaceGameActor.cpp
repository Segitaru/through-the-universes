// Fill out your copyright notice in the Description page of Project Settings.


#include "InterfaceGameActor.h"

// Add default functionality here for any IInterfaceGameActor functions that are not pure virtual.
EPhysicalSurface IInterfaceGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}
TArray<UStateEffect*> IInterfaceGameActor::GetAllCurrentEffects()
{
	TArray<UStateEffect*> Effect;
	return Effect;
}

void IInterfaceGameActor::RemoveEffect(UStateEffect* RemoveEffect)
{

}

void IInterfaceGameActor::AddEffect(UStateEffect* newEffect)
{

}

void IInterfaceGameActor::GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone)
{
}