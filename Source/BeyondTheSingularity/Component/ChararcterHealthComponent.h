// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Component/HealthComponent.h"
#include "ChararcterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, ShieldValue, float, Damage);

UCLASS()
class BEYONDTHESINGULARITY_API UChararcterHealthComponent : public UHealthComponent
{
	GENERATED_BODY()
	
protected:
	float Shield = 100.f;
public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CooldownRecoveryHealth;
	FTimerHandle TimerHandle_RateHealthRecovery;
	FTimerHandle TimerHandle_CooldownRecoveryShield;
	FTimerHandle TimerHandle_RateShieldRecovery;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Coefficient")
		float fDamageCoefShield = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CooldownRecoveryHealth = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CooldownRecoveryShield = 3.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float RateHealthRecovery = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float RateShieldRecovery = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float ValueRecoveryHealth = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float ValueRecoveryShield = 20.f;

	virtual void ChangeCurrentHealth(float ChangeValue) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetCurrentShield();

	void ChangeCurrentShield(float ChangeValue);
	void ShieldCooldownToRecovery();
	void ShieldRecovery();
	void HealthCooldownToRecovery();
	void HealthRecovery();
};
