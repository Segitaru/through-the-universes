// Fill out your copyright notice in the Description page of Project Settings.


#include "../Component/HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UHealthComponent::ChangeCurrentHealth(float ChangeValue)
{
	Health += ChangeValue * fDamageCoef;
	OnHealthChange.Broadcast(Health, ChangeValue);
	if (Health > 100.f)
	{
		Health = 100.f;
	}
	else
	{
		if (Health <= 0.f)
		{
			Health = 0.f;
			OnDead.Broadcast();
		}
	}

}


