// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "InterfaceGameActor.h"
#include "Component/HealthComponent.h"

bool UStateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	IInterfaceGameActor* myInterface = Cast<IInterfaceGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
		if (ParticleEffect)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0);
			USceneComponent* AttachedComponent = nullptr;
			myInterface->GetInfoToAttachEmiterEffect(AttachedComponent, Loc, NameBoneToAttached);
			if (AttachedComponent)
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, AttachedComponent, NameBoneToAttached, Loc, FRotator::ZeroRotator, ScaleEmitter, EAttachLocation::SnapToTarget, false);
		}
	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect::DestroyObject, Timer, false);

	return true;
}



void UStateEffect::DestroyObject()
{
	IInterfaceGameActor* myInterface = Cast<IInterfaceGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}

}

bool UStateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UHealthComponent* myHealthComp = Cast<UHealthComponent>(myActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentHealth(Power);
		}
	}

}

bool UStateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Execute, RateTime, true);


	return true;
}

void UStateEffect_ExecuteTimer::DestroyObject()
{

	Super::DestroyObject();
}

void UStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UHealthComponent* myHealthComp = Cast<UHealthComponent>(myActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentHealth(Power);
		}
	}
}
