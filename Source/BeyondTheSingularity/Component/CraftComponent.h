// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Types.h"
#include "CraftComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnResourceChange, ECraftResource, TypeResource, int32, cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnResourceEmpty, ECraftResource, TypeResource);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnResourceNeedMore, ECraftResource, TypeResource);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnResourceOk, ECraftResource, TypeResource);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartCraft);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndCraft);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BEYONDTHESINGULARITY_API UCraftComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCraftComponent();
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Craft")
		FOnResourceChange OnResourceChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Craft")
		FOnResourceEmpty OnResourceEmpty;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Craft")
		FOnResourceNeedMore OnResourceNeedMore;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Craft")
		FOnResourceOk OnResourceOk;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Craft")
		FOnStartCraft OnStartCraft;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Craft")
		FOnEndCraft OnEndCraft;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	TMap<ECraftResource, int32> ResourceMap;
	TArray<ECraftResource> ResourceType;

	bool bIsCrafting = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Resource")
		TArray<FResourceSlot> ResourceSlot;
	//UFUNCTION(BlueprintCallable)
	//bool CheckCanTakeResource(ECraftResources TypeResource);
	UFUNCTION(BlueprintCallable)
	void ChangeResourceByValue(ECraftResource TypeResource, int32 cout, TMap<ECraftResource, int32> NeedToCraft, bool bIsCollect);

	FTimerHandle TimerHandle_StartCraft;
	float CraftTime_Ammo = 0.5f;
	float CraftTime_Module = 0.5f;
	UFUNCTION(BlueprintCallable)
		void CraftAmmoStart();
	UFUNCTION(BlueprintCallable)
		void CraftModuleStart(TMap<ECraftResource, int32> NeedToCraft);
	UFUNCTION(BlueprintCallable)
		void CraftModuleComplite();

	bool CheckResourceToCraft(TMap<ECraftResource, int32> NeedToCraft);

	void CraftAmmoComplite();

	
	
};
