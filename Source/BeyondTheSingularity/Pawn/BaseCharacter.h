// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Types.h"
#include "../StateEffect.h"
#include "../InterfaceGameActor.h"
#include "BaseCharacter.generated.h"

UCLASS()
class BEYONDTHESINGULARITY_API ABaseCharacter : public ACharacter , public IInterfaceGameActor
{
	GENERATED_BODY()

public:
	
	ABaseCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
		float CameraHeight;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		bool Smooth;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float SmoothFactor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float MaxDistanceCamera = 1500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float MinDistanceCamera = 450.f;
	float CameraAngle = 0.0f;
	FTimerHandle TimerCameraSmooth;
	void SetTimerSmoothCamera();
	void SmoothingCameraBoom();
	USpringArmComponent* GetCameraBoom();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UChararcterHealthComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCraftComponent* CraftComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::RunState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed SpeedState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bWalkState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bSprintState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bAimState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool bRightHandState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool bMeleeWeaponState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeathState")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeathState")
		TArray<UAnimMontage*> DeathMontage;

	FTimerHandle TimerHandle_Rackdoll;
	void EnableRackdoll();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		FName InitWeaponName;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Weapon")
		ABaseWeapon* CurrentWeapon;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
		bool bReUseWeapon = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
		bool bUseWeapon = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		int32 CurrentWeaponIndex = 0;





protected:
	
	virtual void BeginPlay() override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	void ChangeCameraDistance(float AxisValue);

public:	
	UFUNCTION(BlueprintCallable)
		virtual void CharackterUpdate();
	UFUNCTION(BlueprintCallable)
		virtual void ChangeMovementState();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		virtual void InitWeapon(FName IdWeapon, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 Index);
	UFUNCTION(BlueprintCallable)
		virtual void CharacterAttackEvent(bool bIsAttacking);

	UFUNCTION(BlueprintCallable)
		class ABaseWeapon* GetCurrentWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* AnimReload, float ReUseTime);
	UFUNCTION()
		virtual void WeaponUsedStart(UAnimMontage* AnimUsingWeapon, float UsingTime);
	UFUNCTION()
		virtual void WeaponReloadEnd(bool IsSuccess, int32 AmmoSafe);
	UFUNCTION()
		void WeaponUseEnd(bool IsSuccess);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Craft")
		bool bCrafting = false;
	//����������
	TArray<class UStateEffect*> Effects;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Effect")
		FVector EffectOffset = FVector(0);


	EPhysicalSurface GetSurfaceType() override;
	TArray<UStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffect* RemoveEffect) override;
	void AddEffect(UStateEffect* newEffect) override;
	void GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone) override;
	//����������

	UFUNCTION()
		virtual void DeathCharacter();


	void PressedWalkCharacter();
	void PressedSprintCharacter();
	void PressedAimCharacter();
	void PressedAttackCharacter();
	void PressedTryCooldownWeapon();
	void PressedNextWeapon();
	void PressedPreviousWeapon();

	void ReleasedWalkCharacter();
	void ReleasedSprintCharacter();
	void ReleasedAimCharacter();
	void ReleasedAttackCharacter();
};
