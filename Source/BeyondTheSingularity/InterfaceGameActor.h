// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "StateEffect.h"
#include "InterfaceGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInterfaceGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class BEYONDTHESINGULARITY_API IInterfaceGameActor
{
	GENERATED_BODY()

public:
		virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UStateEffect* RemoveEffect);
	virtual void AddEffect(UStateEffect* newEffect);
	virtual void GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone);
};