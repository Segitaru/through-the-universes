// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BeyondTheSingularityGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BEYONDTHESINGULARITY_API ABeyondTheSingularityGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
