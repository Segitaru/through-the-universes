// Fill out your copyright notice in the Description page of Project Settings.


#include "../Pawn/BaseCharacter.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

#include "../Component/ChararcterHealthComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../Component/InventoryComponent.h"
#include "../Component/CraftComponent.h"

#include "../Game/GameInstanceBase.h"
#include "../Weapon/BaseWeapon.h"
#include "../Weapon/ProjectileBasse.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = CameraHeight;
	CameraBoom->SetRelativeRotation(FRotator(CameraAngle, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; 
	CameraBoom->bEnableCameraLag = Smooth;
	CameraBoom->CameraLagSpeed = SmoothFactor;

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = true;
	CraftComponent = CreateDefaultSubobject<UCraftComponent>(TEXT("CraftComponent"));
	HealthComponent = CreateDefaultSubobject<UChararcterHealthComponent>(TEXT("HealthComponent"));
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ABaseCharacter::InitWeapon);
	}
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ABaseCharacter::DeathCharacter);
	}
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	/*auto ReferenceGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	FAddicionalWeaponInfo WeaponInfo;
	FWeaponInfo InfoWeapon;

	if (ReferenceGI)
		ReferenceGI->GetWeaponInfoByName(InitWeaponName, InfoWeapon);
		WeaponInfo.Round = InfoWeapon.MaxRound;
	InitWeapon(InitWeaponName, WeaponInfo, 0);*/
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("CameraDistanceChange", this, &ABaseCharacter::ChangeCameraDistance);
	PlayerInputComponent->BindAction("AimState", IE_Pressed, this, &ABaseCharacter::PressedAimCharacter);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ABaseCharacter::PressedSprintCharacter);
	PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &ABaseCharacter::PressedWalkCharacter);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ABaseCharacter::PressedAttackCharacter);
	PlayerInputComponent->BindAction("CooldownWeapon", IE_Pressed, this, &ABaseCharacter::PressedTryCooldownWeapon);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &ABaseCharacter::PressedNextWeapon);
	PlayerInputComponent->BindAction("PreviousWeapon", IE_Pressed, this, &ABaseCharacter::PressedPreviousWeapon);

	PlayerInputComponent->BindAction("AimState", IE_Released, this, &ABaseCharacter::ReleasedAimCharacter);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ABaseCharacter::ReleasedSprintCharacter);
	PlayerInputComponent->BindAction("Walk", IE_Released, this, &ABaseCharacter::ReleasedWalkCharacter);
	PlayerInputComponent->BindAction("Attack", IE_Released, this, &ABaseCharacter::ReleasedAttackCharacter);
}

void ABaseCharacter::ChangeCameraDistance(float AxisValue)
{
		if (AxisValue != 0)
		{
			if (AxisValue > 0)
			{
				CameraHeight = CameraHeight + 100;
				SetTimerSmoothCamera();
			}
			else
			{
				CameraHeight = CameraHeight - 100;
				SetTimerSmoothCamera();
			}
		}
}

void ABaseCharacter::SetTimerSmoothCamera()
{
	GetWorld()->GetTimerManager().SetTimer(TimerCameraSmooth, this, &ABaseCharacter::SmoothingCameraBoom, 0.01667f, true);
}
void ABaseCharacter::SmoothingCameraBoom()
{

	if (GetCameraBoom()->TargetArmLength < MaxDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = UKismetMathLibrary::FInterpTo(GetCameraBoom()->TargetArmLength, CameraHeight, 1.f, 0.02f);

	}
	if (GetCameraBoom()->TargetArmLength > MaxDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = MaxDistanceCamera;
		GetWorld()->GetTimerManager().ClearTimer(TimerCameraSmooth);

	}
	if (GetCameraBoom()->TargetArmLength > MinDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = UKismetMathLibrary::FInterpTo(GetCameraBoom()->TargetArmLength, CameraHeight, 1.f, 0.02f);
	}
	if (GetCameraBoom()->TargetArmLength < MinDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = MinDistanceCamera;
		GetWorld()->GetTimerManager().ClearTimer(TimerCameraSmooth);

	}
}

float ABaseCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{

	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		if (HealthComponent)
		{
			HealthComponent->ChangeCurrentHealth(-DamageAmount);

		}
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileBasse* myProjectile = Cast<AProjectileBasse>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}
	return ActualDamage;
}
// ������

void ABaseCharacter::InitWeapon(FName IdWeapon, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 Index)
{
	
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	FWeaponInfo WeaponInfo;

	auto ReferenceGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	if (ReferenceGI)
	{

		if (ReferenceGI->GetWeaponInfoByName(IdWeapon, WeaponInfo))
		{
		
			if (WeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);
				FActorSpawnParameters SpawnParam;
				SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParam.Owner = this;
				SpawnParam.Instigator = GetInstigator();

				ABaseWeapon* MyWeapon = Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParam));
				
				if (MyWeapon)
				{
					
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);

					// ���� ������� ������ (�����/������ ����)
					if (WeaponInfo.RightHand == true || WeaponInfo.MeleeWeapon == true)
					{
						MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSlot"));
						bRightHandState = true;
					}
					else
					{
						MyWeapon->AttachToComponent(GetMesh(), Rule, FName("LeftHandSocket"));
						bRightHandState = false;
					}
					
					CurrentWeapon = MyWeapon;
					bMeleeWeaponState = WeaponInfo.MeleeWeapon;
					CurrentWeaponIndex = Index;
					MyWeapon->WeaponSetting = WeaponInfo;
					MyWeapon->WeaponInfo = AddicionalWeaponInfo;
					MyWeapon->UpdateStateWeapon(MovementState);
					if (InventoryComponent)
					{
						InventoryComponent->OnAmmoWeaponAvaible.Broadcast(CurrentWeapon->WeaponSetting.WeaponType);
					}
					/* �������� � ��������� */ {
						MyWeapon->OnWeapopReloadStart.AddDynamic(this, &ABaseCharacter::WeaponReloadStart);
						MyWeapon->OnWeapopReloadEnd.AddDynamic(this, &ABaseCharacter::WeaponReloadEnd);
						MyWeapon->OnWeapopUseStart.AddDynamic(this, &ABaseCharacter::WeaponUsedStart);
						MyWeapon->OnWeapopUseEnd.AddDynamic(this, &ABaseCharacter::WeaponUseEnd);
						if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CanReuse())
						{
							CurrentWeapon->InitCooldown();
						}


					}

				}
			}
		}


	}
}
void ABaseCharacter::CharacterAttackEvent(bool bIsAttacking)
{
	ABaseWeapon* MyWeapon = nullptr;
	MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire(bIsAttacking);
	}
}

// ������

// Change State

void ABaseCharacter::CharackterUpdate()
{
	float ResultSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::AimState:
		ResultSpeed = SpeedState.AimSpeed;
		break;
	case EMovementState::WalkState:
		ResultSpeed = SpeedState.WalkSpeed;
		break;
	case EMovementState::RunState:
		ResultSpeed = SpeedState.RunSpeed;
		break;
	case EMovementState::SprintState:
		ResultSpeed = SpeedState.SprintSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;

}

void ABaseCharacter::ChangeMovementState()
{
	if (!bWalkState && !bSprintState && !bAimState)
	{
		MovementState = EMovementState::RunState;
	}
	else
	{
		if (bSprintState)
		{
			bWalkState = false;
			bAimState = false;
			MovementState = EMovementState::SprintState;
		}
		if (bWalkState && !bSprintState && !bAimState)
		{
			MovementState = EMovementState::WalkState;
		}
		else if (bAimState)
		{
			bWalkState = false;
			MovementState = EMovementState::AimState;
		}
	}
	CharackterUpdate();
	ABaseWeapon* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->UpdateStateWeapon(MovementState);
	}
}


USpringArmComponent* ABaseCharacter::GetCameraBoom()
{
	return CameraBoom;
}



ABaseWeapon* ABaseCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ABaseCharacter::WeaponReloadStart(UAnimMontage* AnimReload, float ReUseTime)
{
	if (ReUseTime <= 1.f)
	{
		if (AnimReload && ReUseTime)
			PlayAnimMontage(AnimReload, 1.0f / (ReUseTime));
	}
	else
	{
		if (AnimReload && ReUseTime)
			PlayAnimMontage(AnimReload, (ReUseTime - 1.f) / (ReUseTime + 1.f));
	}
	bReUseWeapon = true;
}

void ABaseCharacter::WeaponReloadEnd(bool IsSuccess, int32 AmmoSafe)
{
	if (IsSuccess)
	{
		bReUseWeapon = false;
		if (InventoryComponent)
		{
			InventoryComponent->WeaponChangeAmmo(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
			InventoryComponent->SetAddicionalWeaponInfo(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
		}
	}
	else
	{
		StopAnimMontage();
		bReUseWeapon = false;
	}
}



void ABaseCharacter::WeaponUsedStart(UAnimMontage* AnimUsingWeapon, float UsingTime)
{
	if (InventoryComponent && CurrentWeapon)
	{

		InventoryComponent->SetAddicionalWeaponInfo(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
	}
	if (UsingTime <= 1.f)
	{
		if (AnimUsingWeapon && UsingTime)
			PlayAnimMontage(AnimUsingWeapon, 1.0f / (UsingTime));
	}
	else
	{
		if (AnimUsingWeapon && UsingTime)
			PlayAnimMontage(AnimUsingWeapon, (UsingTime - 1.f) / (UsingTime + 1.f));
	}
	bUseWeapon = true;

}

void ABaseCharacter::WeaponUseEnd(bool IsSuccess)
{
	bUseWeapon = false;
}







void ABaseCharacter::PressedWalkCharacter()
{
	bWalkState = true;
	ChangeMovementState();
}
void ABaseCharacter::PressedSprintCharacter()
{
	bSprintState = true;
}
void ABaseCharacter::PressedAimCharacter()
{
	bAimState = true;
	ChangeMovementState();
}
void ABaseCharacter::PressedAttackCharacter()
{
	if(!bCrafting)
	CharacterAttackEvent(true);
	
}
void ABaseCharacter::PressedTryCooldownWeapon()
{
	if (GetCurrentWeapon() && !GetCurrentWeapon()->bWeaponCooldown)
	{

		if (GetCurrentWeapon()->GetWeaponRound() < GetCurrentWeapon()->WeaponSetting.MaxRound && GetCurrentWeapon()->CanReuse())
		{
			GetCurrentWeapon()->InitCooldown();

		}
	}
}
void ABaseCharacter::PressedNextWeapon()
{
	if (InventoryComponent->WeaponsSlot.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->bWeaponCooldown)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentWeaponIndex + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}
void ABaseCharacter::PressedPreviousWeapon()
{
	if (InventoryComponent->WeaponsSlot.Num() > 1)
	{
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->bWeaponCooldown)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentWeaponIndex - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void ABaseCharacter::ReleasedWalkCharacter()
{
	bWalkState = false;
	ChangeMovementState();
}
void ABaseCharacter::ReleasedSprintCharacter()
{
	bSprintState = false;
	ChangeMovementState();
}
void ABaseCharacter::ReleasedAimCharacter()
{
	bAimState = false;
	ChangeMovementState();
}
void ABaseCharacter::ReleasedAttackCharacter()
{
	CharacterAttackEvent(false);
}



void ABaseCharacter::DeathCharacter()
{
	bIsAlive = false;
	float TimeDeathAnim = 0.f;
	int32 RandomInt = FMath::RandHelper(DeathMontage.Num());
	if (DeathMontage.IsValidIndex(RandomInt) && DeathMontage[RandomInt] && GetMesh()->GetAnimInstance())
	{
		TimeDeathAnim = DeathMontage[RandomInt]->GetPlayLength();
		GetMesh()->GetAnimInstance()->StopAllMontages(0.2f);
		GetMesh()->GetAnimInstance()->Montage_Play(DeathMontage[RandomInt]);

	}
	UnPossessed();

	GetWorldTimerManager().SetTimer(TimerHandle_Rackdoll, this, &ABaseCharacter::EnableRackdoll, TimeDeathAnim, false);
}

void ABaseCharacter::EnableRackdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}














EPhysicalSurface ABaseCharacter::GetSurfaceType()
{
	EPhysicalSurface SurfaceResult = EPhysicalSurface::SurfaceType_Default;
	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					SurfaceResult = myMaterial->GetPhysicalMaterial()->SurfaceType;

				}
			}
		}
	}

	return SurfaceResult;
}

TArray<UStateEffect*> ABaseCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ABaseCharacter::RemoveEffect(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ABaseCharacter::AddEffect(UStateEffect* newEffect)
{
	Effects.Add(newEffect);
}

void ABaseCharacter::GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone)
{
	if (GetMesh())
	{
		Component = GetMesh();
		Offset = EffectOffset;
		AttachedBone = "spine_03";
	}
}