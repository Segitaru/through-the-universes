// Fill out your copyright notice in the Description page of Project Settings.


#include "../Component/InventoryComponent.h"
#include "../Game/GameInstanceBase.h"
#include "Engine/StaticMeshActor.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	MaxSlotWeapon = WeaponsSlot.Num();
	FWeaponInfo WeaponInfo;
	UGameInstanceBase* MyGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		for (int8 i = 0; i < WeaponsSlot.Num(); i++)
		{
			if (!WeaponsSlot[i].NameItem.IsNone())
			{

				MyGI->GetWeaponInfoByName(WeaponsSlot[i].NameItem, WeaponInfo);
				WeaponsSlot[i].AddicionalWeaponInfo.Round = WeaponInfo.MaxRound;
			}
			else
			{
			
			}
		}

	}

	if (WeaponsSlot.IsValidIndex(0))
	{
		if (!WeaponsSlot[0].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponsSlot[0].NameItem, WeaponsSlot[0].AddicionalWeaponInfo, 0);
		}
	}

}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//����������� ��������� �� ������ � ����������� � ������ �������
bool UInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAddicionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponsSlot.Num() - 1)
	{
		CorrectIndex = 0;
	}
	else if (ChangeToIndex < 0)
	{
		CorrectIndex = WeaponsSlot.Num() - 1;
	}
	FName NewIdWeapon;
	FAddicionalWeaponInfo NewAddicionalWeaponInfo;
	int32 NewCurrentIndex = 0;


	if (WeaponsSlot.IsValidIndex(CorrectIndex))
	{
		if (!WeaponsSlot[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponsSlot[CorrectIndex].AddicionalWeaponInfo.Round > 0)
			{
				//good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UGameInstanceBase* myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//check ammoSlots for this weapon
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponsSlot[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponsSlot[CorrectIndex].NameItem;
				NewAddicionalWeaponInfo = WeaponsSlot[CorrectIndex].AddicionalWeaponInfo;
			}
		}
	}

	if (!bIsSuccess)
	{
		if (bIsForward)
		{
			int8 iteration = 0;
			int8 Seconditeration = 0;
			while (iteration < WeaponsSlot.Num() && !bIsSuccess)
			{
				iteration++;
				int8 tmpIndex = ChangeToIndex + iteration;
				if (WeaponsSlot.IsValidIndex(tmpIndex))
				{
					if (!WeaponsSlot[tmpIndex].NameItem.IsNone())
					{
						if (WeaponsSlot[tmpIndex].AddicionalWeaponInfo.Round > 0)
						{
							//WeaponGood
							bIsSuccess = true;
							NewIdWeapon = WeaponsSlot[tmpIndex].NameItem;
							NewAddicionalWeaponInfo = WeaponsSlot[tmpIndex].AddicionalWeaponInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UGameInstanceBase* myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponsSlot[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponsSlot[tmpIndex].NameItem;
									NewAddicionalWeaponInfo = WeaponsSlot[tmpIndex].AddicionalWeaponInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of right of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponsSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponsSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponsSlot[Seconditeration].AddicionalWeaponInfo.Round > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponsSlot[Seconditeration].NameItem;
									NewAddicionalWeaponInfo = WeaponsSlot[Seconditeration].AddicionalWeaponInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UGameInstanceBase* myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponsSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//WeaponGood
											bIsSuccess = true;
											NewIdWeapon = WeaponsSlot[Seconditeration].NameItem;
											NewAddicionalWeaponInfo = WeaponsSlot[Seconditeration].AddicionalWeaponInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponsSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponsSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponsSlot[Seconditeration].AddicionalWeaponInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;
									UGameInstanceBase* myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponsSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												UE_LOG(LogTemp, Error, TEXT("INFINITY GUN NEED UInventoryComponent::TrySwitchWeapon"));
											}
										}
										j++;
									}
								}
							}
						}
					}
					Seconditeration++;
				}
			}
		}
		else
		{
			int8 iteration = 0;
			int8 Seconditeration = WeaponsSlot.Num() - 1;
			while (iteration < WeaponsSlot.Num() && !bIsSuccess)
			{
				iteration++;
				int8 tmpIndex = ChangeToIndex - iteration;
				if (WeaponsSlot.IsValidIndex(tmpIndex))
				{
					if (!WeaponsSlot[tmpIndex].NameItem.IsNone())
					{
						if (WeaponsSlot[tmpIndex].AddicionalWeaponInfo.Round > 0)
						{
							//WeaponGood
							bIsSuccess = true;
							NewIdWeapon = WeaponsSlot[tmpIndex].NameItem;
							NewAddicionalWeaponInfo = WeaponsSlot[tmpIndex].AddicionalWeaponInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UGameInstanceBase* myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponsSlot[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponsSlot[tmpIndex].NameItem;
									NewAddicionalWeaponInfo = WeaponsSlot[tmpIndex].AddicionalWeaponInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of LEFT of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponsSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponsSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponsSlot[Seconditeration].AddicionalWeaponInfo.Round > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponsSlot[Seconditeration].NameItem;
									NewAddicionalWeaponInfo = WeaponsSlot[Seconditeration].AddicionalWeaponInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UGameInstanceBase* myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponsSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//WeaponGood
											bIsSuccess = true;
											NewIdWeapon = WeaponsSlot[Seconditeration].NameItem;
											NewAddicionalWeaponInfo = WeaponsSlot[Seconditeration].AddicionalWeaponInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponsSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponsSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponsSlot[Seconditeration].AddicionalWeaponInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;
									UGameInstanceBase* myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponsSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												UE_LOG(LogTemp, Error, TEXT("INFINITY GUN NEED UInventoryComponent::TrySwitchWeapon"));
											}
										}
										j++;
									}
								}
							}
						}
					}
					Seconditeration--;
				}
			}
		}
	}

	if (bIsSuccess)
	{
		SetAddicionalWeaponInfo(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAddicionalWeaponInfo, NewCurrentIndex);
	}

	return bIsSuccess;
}

FAddicionalWeaponInfo UInventoryComponent::GetFAddicionalWeaponInfo(int32 IndexWeapon)
{
	FAddicionalWeaponInfo result;
	if (WeaponsSlot.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponsSlot.Num() && !bIsFind)
		{
			if (/*WeaponsSlot[i].SlotIndex*/i == IndexWeapon)
			{
				result = WeaponsSlot[i].AddicionalWeaponInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{

		}
	}

	return result;
}

int32 UInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	bool bIsFind = false;
	int8 i = 0;
	while (i < WeaponsSlot.Num() && !bIsFind)
	{
		if (WeaponsSlot[i].NameItem == IdWeaponName)
		{
			result = i;
			bIsFind = true;
		}
		i++;
	}


	return result;
}

void UInventoryComponent::SetAddicionalWeaponInfo(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo)
{
	if (WeaponsSlot.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponsSlot.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponsSlot[i].AddicionalWeaponInfo = NewInfo;
				bIsFind = true;
				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
		{

		}
	}

}

void UInventoryComponent::WeaponChangeAmmo(EWeaponType TypeWeapon, int32 AmmoChange)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += AmmoChange;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
			{
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}
			OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			bIsFind = true;
			if (AmmoSlots[i].Cout > 0)
				OnAmmoWeaponAvaible.Broadcast(AmmoSlots[i].WeaponType);
		}
		i++;
	}
}

bool UInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 & AviableAmmoToReload)
{
	AviableAmmoToReload = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoToReload = AmmoSlots[i].Cout;
			if (AmmoSlots[i].Cout > 0)
			{


				return true;
			}
		}
		i++;

	}
	OnAmmoWeaponEmpty.Broadcast(TypeWeapon);
	return false;
}

bool UInventoryComponent::CheckCanTakeWeapon(int32 & FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponsSlot.Num() && !bIsFreeSlot)
	{
		if (WeaponsSlot[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
		{
			result = true;
		}
		i++;
	}
	return result;
}

bool UInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem & DropItemInfo)
{
	bool result = false;

	if (WeaponsSlot.IsValidIndex(IndexSlot) && GetDropItemFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponsSlot[IndexSlot] = NewWeapon;
		SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AddicionalWeaponInfo, true);
		OnUpdateWeaponSlot.Broadcast(IndexSlot, NewWeapon);

		//��� �����������
		FWeaponInfo WeaponInfo;
		FWeaponSlot TryWeapon;
		UGameInstanceBase* MyGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
		if (MyGI)
		{
			MyGI->GetWeaponInfoByName(NewWeapon.NameItem, WeaponInfo);
			if (NewWeapon.AddicionalWeaponInfo.Round > WeaponInfo.MaxRound)
			{
				TryWeapon.AddicionalWeaponInfo.Round = WeaponInfo.MaxRound;
				TryWeapon.NameItem = NewWeapon.NameItem;
				OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, TryWeapon.AddicionalWeaponInfo);
			}
			else
			{
				OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, NewWeapon.AddicionalWeaponInfo);
			}

		}
		// 

		result = true;
	}
	return result;
}


FName UInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName NameWeapon;
	if (WeaponsSlot.IsValidIndex(IndexSlot))
	{
		NameWeapon = WeaponsSlot[IndexSlot].NameItem;
	}
	return NameWeapon;
}

bool UInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot))
	{
		if (WeaponsSlot.IsValidIndex(IndexSlot))
		{ //��� �����������
			WeaponsSlot[IndexSlot] = NewWeapon;
			FWeaponInfo WeaponInfo;
			FWeaponSlot TryWeapon;
			UGameInstanceBase* MyGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
			if (MyGI)
			{
				MyGI->GetWeaponInfoByName(NewWeapon.NameItem, WeaponInfo);
				if (NewWeapon.AddicionalWeaponInfo.Round > WeaponInfo.MaxRound)
				{
					TryWeapon.AddicionalWeaponInfo.Round = WeaponInfo.MaxRound;
					TryWeapon.NameItem = NewWeapon.NameItem;
					OnUpdateWeaponSlot.Broadcast(IndexSlot, NewWeapon);
					OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, TryWeapon.AddicionalWeaponInfo);
				}
				else
				{
					OnUpdateWeaponSlot.Broadcast(IndexSlot, NewWeapon);
					OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, NewWeapon.AddicionalWeaponInfo);
				}
				return true;
			}

			//


		}
	}
	return false;
}
bool UInventoryComponent::GetDropItemFromInventory(int32 IndexSlot, FDropItem & DropItemInfo)
{
	bool result = false;
	/*FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	auto myGI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	if (myGI)
	{

		result = myGI->GetDropInfoByName(DropItemName, DropItemInfo);
		if (WeaponsSlot.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponSlotInfo.AddicionalWeaponInfo = WeaponsSlot[IndexSlot].AddicionalWeaponInfo;
		}

	}*/
	return result;
}
#pragma optimize ("", on)