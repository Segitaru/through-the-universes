// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/ProjectileBasse.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
// Sets default values
AProjectileBasse::AProjectileBasse()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	RootComponent = SphereCollision;
	SphereCollision->SetSphereRadius(16.f);
	SphereCollision->SetCollisionProfileName("Projectile");

	SphereCollision->bReturnMaterialOnMove = true;
	SphereCollision->SetCanEverAffectNavigation(false);


	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	StaticMeshComponent->SetupAttachment(SphereCollision);
	StaticMeshComponent->SetCanEverAffectNavigation(false);

	ProjectileFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile FX"));
	ProjectileFX->SetupAttachment(SphereCollision);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovementComponent->UpdatedComponent = RootComponent;
	ProjectileMovementComponent->InitialSpeed = ProjectileSetting.fProjectileInitSpeed;
	ProjectileMovementComponent->MaxSpeed = ProjectileSetting.fProjectileInitSpeed;

	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = true;
	ProjectileMovementComponent->bInitialVelocityInLocalSpace = false;
}

// Called when the game starts or when spawned
void AProjectileBasse::BeginPlay()
{
	Super::BeginPlay();
	SphereCollision->OnComponentHit.AddDynamic(this, &AProjectileBasse::ProjectileCollisionSphereHit);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AProjectileBasse::ProjectileCollisionSphereBeginOverlap);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &AProjectileBasse::ProjectileCollisionSphereEndOverlap);


	
}

// Called every frame
void AProjectileBasse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileBasse::InitProjectile(FProjectileInfo InitParam)
{
	ProjectileSetting = InitParam;
	ProjectileMovementComponent->InitialSpeed = ProjectileSetting.fProjectileInitSpeed;
	ProjectileMovementComponent->MaxSpeed = ProjectileSetting.fProjectileInitSpeed;
	ProjectileMovementComponent->ProjectileGravityScale = ProjectileSetting.fProjectileGravityScale;
	this->SetLifeSpan(ProjectileSetting.fProjectileLifeTime);
	if (ProjectileSetting.ProjectileMesh)
	{
		StaticMeshComponent->SetStaticMesh(ProjectileSetting.ProjectileMesh);
		StaticMeshComponent->SetRelativeTransform(ProjectileSetting.ProjectileMeshTransform);
	}
	else
	{
		StaticMeshComponent->DestroyComponent();
	}
	if (ProjectileSetting.ProjectileTrialFX)
	{
		ProjectileFX->SetTemplate(ProjectileSetting.ProjectileTrialFX);
		ProjectileFX->SetRelativeTransform(ProjectileSetting.ProjectileTrialFXTransform);
	}
	else
	{
		ProjectileFX->DestroyComponent();
	}
	ProjectileMovementComponent->Velocity = VelocityInit;

}

void AProjectileBasse::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
		if (ProjectileSetting.HitDecals.Contains(MySurfaceType))
		{
			UMaterialInterface* MyMaterial = ProjectileSetting.HitDecals[MySurfaceType];
			if (MyMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(MyMaterial, FVector(20.f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}
		if (ProjectileSetting.HitFXs.Contains(MySurfaceType))
		{
			UParticleSystem* MyParticle = ProjectileSetting.HitFXs[MySurfaceType];
			if (MyParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MyParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}
		//ICOVInterfaceGameActor* myInterface = Cast<ICOVInterfaceGameActor>(Hit.GetActor());
		//if (myInterface)
		//{
		UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileSetting.Effect, MySurfaceType);
		//}
	}
	if (ProjectileSetting.ProjectileHitSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ProjectileHitSound, Hit.ImpactPoint);
	}
	UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.fProjectileDamage, GetInstigatorController(), this, NULL);
	ImpactProjectile();
}

void AProjectileBasse::ProjectileCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AProjectileBasse::ProjectileCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileBasse::ImpactProjectile()
{
	this->Destroy();
}


