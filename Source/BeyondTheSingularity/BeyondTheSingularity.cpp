// Copyright Epic Games, Inc. All Rights Reserved.

#include "BeyondTheSingularity.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BeyondTheSingularity, "BeyondTheSingularity" );
