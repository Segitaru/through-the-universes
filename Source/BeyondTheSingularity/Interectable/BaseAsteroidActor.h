// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Interectable/BaseInterectableActor.h"
#include "BaseAsteroidActor.generated.h"

/**
 * 
 */
UCLASS()
class BEYONDTHESINGULARITY_API ABaseAsteroidActor : public ABaseInterectableActor
{
	GENERATED_BODY()
	
};
