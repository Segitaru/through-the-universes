// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Weapon/ProjectileBasse.h"
#include "Projectile_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class BEYONDTHESINGULARITY_API AProjectile_Grenade : public AProjectileBasse
{
	GENERATED_BODY()
public:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	void Exploud();
	virtual void ProjectileCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void ImpactProjectile() override;
	void TimerEsploded(float DeltaTime);
	bool bTimerEnable = false;
	float TimerExploud = 0.0f;
	float TimeToExploud = 0.1f;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX, meta = (AllowPrivateAccess = "true"))
		float fSizeFX = 1;
};