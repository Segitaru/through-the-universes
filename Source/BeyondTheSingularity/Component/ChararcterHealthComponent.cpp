// Fill out your copyright notice in the Description page of Project Settings.


#include "../Component/ChararcterHealthComponent.h"

void UChararcterHealthComponent::ChangeCurrentHealth(float ChangeValue)
{
	if (ChangeValue < 0)
	{
		if (Shield > 0)
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryShield);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryHealth);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateHealthRecovery);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateShieldRecovery);

			ChangeCurrentShield(ChangeValue);

			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownRecoveryShield, this, &UChararcterHealthComponent::ShieldCooldownToRecovery, CooldownRecoveryShield, false);
		}
		else
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryShield);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryHealth);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateHealthRecovery);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateShieldRecovery);

			Super::ChangeCurrentHealth(ChangeValue);

			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownRecoveryShield, this, &UChararcterHealthComponent::ShieldCooldownToRecovery, CooldownRecoveryShield, false);
		}
	}
	else
		Super::ChangeCurrentHealth(ChangeValue);
}

float UChararcterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UChararcterHealthComponent::ChangeCurrentShield(float ChangeValue)
{
	Shield += ChangeValue * fDamageCoef * fDamageCoefShield;
	OnShieldChange.Broadcast(Shield, ChangeValue);
	if (Shield > 100.f)
	{
		Shield = 100.f;
	}
	else
	{
		if (Shield <= 0.f)
		{
			Shield = 0;
		}
	}
}

void UChararcterHealthComponent::ShieldCooldownToRecovery()
{

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_RateShieldRecovery, this, &UChararcterHealthComponent::ShieldRecovery, RateShieldRecovery, true);
}

void UChararcterHealthComponent::ShieldRecovery()
{
	if (Shield < 100.f)
	{
		Shield += ValueRecoveryShield;
		OnShieldChange.Broadcast(Shield, ValueRecoveryShield);
	}
	else
	{
		Shield = 100.f;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateShieldRecovery);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownRecoveryHealth, this, &UChararcterHealthComponent::HealthCooldownToRecovery, CooldownRecoveryHealth, false);

	}

}

void UChararcterHealthComponent::HealthCooldownToRecovery()
{

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_RateHealthRecovery, this, &UChararcterHealthComponent::HealthRecovery, RateHealthRecovery, true);
}

void UChararcterHealthComponent::HealthRecovery()
{

	if (Health < 100.f)
	{
		Health += ValueRecoveryHealth;
		OnHealthChange.Broadcast(Health, ValueRecoveryHealth);
	}
	else
	{
		Health = 100.f;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateHealthRecovery);

	}
}