// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/Projectile_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AProjectile_Grenade::BeginPlay()
{
	Super::BeginPlay();
}
void AProjectile_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerEsploded(DeltaTime);
}

void AProjectile_Grenade::Exploud()
{
	bTimerEnable = false;
	if (ProjectileSetting.ExploudeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploudeFX, GetActorLocation(), GetActorRotation(), GetActorScale() * fSizeFX);
	}
	if (ProjectileSetting.ExploudSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploudSound, GetActorLocation());
	}
	TArray<AActor* > IgnoredActor;

	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.fExploudMaxDamage,
		ProjectileSetting.fExploudMaxDamage * 0.2f, GetActorLocation(),
		ProjectileSetting.fProjectileMaxRadiusDamage,
		ProjectileSetting.fProjectileMinRadiusDamage,
		ProjectileSetting.fCoefDecreaseDamage,
		NULL, IgnoredActor, this, nullptr);

	if (bShowDebug)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.fProjectileMaxRadiusDamage, 8, FColor::Red, false, 4.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.fProjectileMinRadiusDamage, 8, FColor::Green, false, 4.f);
	}

	this->Destroy();
}

void AProjectile_Grenade::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::ProjectileCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectile_Grenade::ImpactProjectile()
{
	bTimerEnable = true;
	TimeToExploud = ProjectileSetting.fTimeToExploud;
}

void AProjectile_Grenade::TimerEsploded(float DeltaTime)
{
	if (bTimerEnable)
	{
		if (TimerExploud >= TimeToExploud)
		{
			Exploud();
		}
		else
		{
			TimerExploud += DeltaTime;
		}
	}
}
