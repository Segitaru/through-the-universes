// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Types.h"
#include "BaseWeapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeapopUseStart, UAnimMontage*, AnimUseWeapon, float, ReUseTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeapopUseEnd, bool, IsSuccess);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeapopReloadStart, UAnimMontage*, AnimReload, float, ReUseTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeapopReloadEnd, bool, IsSuccess, int32, AmmoSafe);

UCLASS()
class BEYONDTHESINGULARITY_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();
	FOnWeapopUseStart OnWeapopUseStart;
	FOnWeapopUseEnd OnWeapopUseEnd;
	FOnWeapopReloadStart OnWeapopReloadStart;
	FOnWeapopReloadEnd OnWeapopReloadEnd;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USkeletalMeshComponent* SkeletalMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* ShootLocation = nullptr;

		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAddicionalWeaponInfo WeaponInfo;

	//Stats
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reload")
		float CooldownTime = 0.0f;
	float fCurrentDispersion = 0.f;
	float fCurrentDispersionMax = 10.f;
	float fCurrentDispersionMin = 0.1f;
	float fCurrentDispersionRecoil = 0.1f;
	float fCurrentDispersionReduction = 0.1f;
	//Stats

	// Other
	FVector EndLocationShoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.f;
	// Other

	//�����
	bool bShouldReduceDispersion = false;
	bool DropMeshFlag = false;
	bool DropOtherMeshFlag = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Use logic")
		bool bWeaponUse = false;
	bool bBlockUse = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Use logic")
		bool bWeaponCooldown = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bShowDebug = false;
	UParticleSystemComponent* TraceEmittor = nullptr;
	bool DestroyTrace = false;
	//�����

	//�������
	float TimeTrace = 0.5f;
	float SpeedRate = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reload")
		float CooldownRate = 0.0f;
	float DropMeshTimer = 0.f;
	float DropOtherMeshTimer = 0.f;

	//�������


protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	void UseWeaponTick(float DeltaTime);
	void CooldownWeaponTick(float DeltaTime);
	void DispertionTick(float DeltaTime);
	void DropOtherMeshTick(float DeltaTime);
	void DropMeshTick(float DeltaTime);

	void WeaponInit();
	void InitCooldown();
	void InitDropMesh(UStaticMesh* MeshToDrop, FTransform Offset, FVector ImpulseDrop, float LifeTime, float ImpulseRandomDespersion, float PowerImpulse, float CustomMass);

	// ������

	void UseWeapon();
	void FinishCooldownToUse();
	void CancelReload();
	// ������

	// Change State

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);
	void UpdateStateWeapon(EMovementState MovementState);
	FVector ApplyDisparcionToShoot(FVector DirectionShoot) const;
	void ChangeDispertionByShot();
	bool CanReuse();
	int8 GetAviableAmmoToReload();

	// Change State

	// GetSome...()

	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound();
	FVector GetShootEndLocation();
	bool CheckWeaponCanUsed();
	FProjectileInfo GetProjectile();
	float GetCurrentDispersion() const;
	int8 GetNumberProjectileByShoot() const;

	// GetSome...()
};
