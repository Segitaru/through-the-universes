// Fill out your copyright notice in the Description page of Project Settings.


#include "../Pawn/Comet.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "../Component/HealthComponent.h"
// Sets default values
AComet::AComet()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxComponent->SetupAttachment(RootComponent);
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = CameraHeight;
	CameraBoom->SetRelativeRotation(FRotator(CameraAngle, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->bEnableCameraLag = Smooth;
	CameraBoom->CameraLagSpeed = SmoothFactor;

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void AComet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AComet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AComet::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("CameraDistanceChange", this, &AComet::ChangeCameraDistance);
}

void AComet::ChangeCameraDistance(float AxisValue)
{
	if (AxisValue != 0)
	{
		if (AxisValue > 0)
		{
			CameraHeight = CameraHeight + 100;
			SetTimerSmoothCamera();
		}
		else
		{
			CameraHeight = CameraHeight - 100;
			SetTimerSmoothCamera();
		}
	}
}

void AComet::SetTimerSmoothCamera()
{
	GetWorld()->GetTimerManager().SetTimer(TimerCameraSmooth, this, &AComet::SmoothingCameraBoom, 0.01667f, true);
}
void AComet::SmoothingCameraBoom()
{

	if (GetCameraBoom()->TargetArmLength < MaxDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = UKismetMathLibrary::FInterpTo(GetCameraBoom()->TargetArmLength, CameraHeight, 1.f, 0.02f);

	}
	if (GetCameraBoom()->TargetArmLength > MaxDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = MaxDistanceCamera;
		GetWorld()->GetTimerManager().ClearTimer(TimerCameraSmooth);

	}
	if (GetCameraBoom()->TargetArmLength > MinDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = UKismetMathLibrary::FInterpTo(GetCameraBoom()->TargetArmLength, CameraHeight, 1.f, 0.02f);
	}
	if (GetCameraBoom()->TargetArmLength < MinDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = MinDistanceCamera;
		GetWorld()->GetTimerManager().ClearTimer(TimerCameraSmooth);

	}
}

USpringArmComponent* AComet::GetCameraBoom()
{
	return CameraBoom;
}
