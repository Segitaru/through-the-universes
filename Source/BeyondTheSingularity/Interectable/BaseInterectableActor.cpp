// Fill out your copyright notice in the Description page of Project Settings.


#include "../Interectable/BaseInterectableActor.h"
#include "../Component/HealthComponent.h"
#include "Components/BoxComponent.h"
// Sets default values
ABaseInterectableActor::ABaseInterectableActor()
{
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxComponent->SetupAttachment(RootComponent);
	BoxComponent->bReturnMaterialOnMove = true;
	BoxComponent->SetCanEverAffectNavigation(false);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	StaticMeshComponent->SetupAttachment(BoxComponent);
	StaticMeshComponent->SetCanEverAffectNavigation(false);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void ABaseInterectableActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseInterectableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

