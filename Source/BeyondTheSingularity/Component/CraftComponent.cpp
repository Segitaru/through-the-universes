// Fill out your copyright notice in the Description page of Project Settings.


#include "../Component/CraftComponent.h"
#include "CraftComponent.h"
#include "../Component/InventoryComponent.h"
#include "../Pawn/BaseCharacter.h"
#include "../Weapon/BaseWeapon.h"

// Sets default values for this component's properties
UCraftComponent::UCraftComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCraftComponent::BeginPlay()
{

	ResourceType = { ECraftResource::Crystal, ECraftResource::Gold, ECraftResource::Metal, ECraftResource::Stone, ECraftResource::Wood };
	Super::BeginPlay();
	for (int i = 0; i < ResourceSlot.Num(); i++)
	{
		ResourceMap.Add(ResourceSlot[i].TypeResource, ResourceSlot[i].cout);
	}
	
}


// Called every frame
void UCraftComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UCraftComponent::ChangeResourceByValue(ECraftResource TypeResource, int32 cout, TMap<ECraftResource, int32> NeedToCraft, bool bIsCollect)
{

	bool bIsFind = false;
	int8 i = 0;
	while (i < ResourceSlot.Num() && !bIsFind)
	{
		if (ResourceSlot[i].TypeResource == TypeResource)
		{
			ResourceSlot[i].cout += cout;
			if(bIsCollect)
				ResourceMap.Add(ResourceSlot[i].TypeResource, ResourceMap.FindRef(ResourceSlot[i].TypeResource) + NeedToCraft.FindRef(ResourceSlot[i].TypeResource));
			else
				ResourceMap.Add(ResourceSlot[i].TypeResource, ResourceMap.FindRef(ResourceSlot[i].TypeResource) - NeedToCraft.FindRef(ResourceSlot[i].TypeResource));
			UE_LOG(LogTemp, Error, TEXT("%i"), ResourceMap.FindRef(ResourceSlot[i].TypeResource));
			OnResourceChange.Broadcast(ResourceSlot[i].TypeResource, ResourceSlot[i].cout);
			bIsFind = true;
			if (ResourceSlot[i].cout <= 0)
				OnResourceEmpty.Broadcast(ResourceSlot[i].TypeResource);
		}
	
		i++;
	}
	
}

void UCraftComponent::CraftAmmoStart()
{
	
	if (!bIsCrafting)
	{
	
		ABaseCharacter* myChar = Cast<ABaseCharacter>(GetOwner());
		if (myChar)
		{
			if (CheckResourceToCraft(myChar->GetCurrentWeapon()->WeaponSetting.NeedToCraft_Ammo))
			{
				bIsCrafting = true;
				myChar->bCrafting = true;
				myChar->CharacterAttackEvent(false);
				OnStartCraft.Broadcast();
				GetWorld()->GetTimerManager().SetTimer(TimerHandle_StartCraft, this, &UCraftComponent::CraftAmmoComplite, CraftTime_Ammo, false);
			}
		
		}
	}
	
}

void UCraftComponent::CraftAmmoComplite()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StartCraft);
	ABaseCharacter* myChar = Cast<ABaseCharacter>(GetOwner());
	if (myChar)
	{
		bIsCrafting = false;
		myChar->bCrafting = false;

		
		for (int i = 0; i < ResourceSlot.Num(); i++)
		{
			ChangeResourceByValue(ResourceSlot[i].TypeResource, -myChar->GetCurrentWeapon()->WeaponSetting.NeedToCraft_Ammo.FindRef(ResourceSlot[i].TypeResource), myChar->GetCurrentWeapon()->WeaponSetting.NeedToCraft_Ammo, false);
		}
		myChar->InventoryComponent->WeaponChangeAmmo(myChar->GetCurrentWeapon()->WeaponSetting.WeaponType, myChar->GetCurrentWeapon()->WeaponSetting.CraftedAmmo);
		myChar->InventoryComponent->SetAddicionalWeaponInfo(myChar->CurrentWeaponIndex, myChar->CurrentWeapon->WeaponInfo);
		myChar->PressedTryCooldownWeapon();
		OnEndCraft.Broadcast();

	}
}

void UCraftComponent::CraftModuleStart(TMap<ECraftResource, int32> NeedToCraft)
{
	if (!bIsCrafting)
	{
		ABaseCharacter* myChar = Cast<ABaseCharacter>(GetOwner());
		if (myChar)
		{
			if (CheckResourceToCraft(NeedToCraft))
			{
				GetWorld()->GetTimerManager().SetTimer(TimerHandle_StartCraft, this, &UCraftComponent::CraftModuleComplite, CraftTime_Module, false);
				bIsCrafting = true;
				myChar->bCrafting = true;
				myChar->CharacterAttackEvent(false);
				OnStartCraft.Broadcast();
	

				for (int i = 0; i < ResourceSlot.Num(); i++)
				{
					ChangeResourceByValue(ResourceSlot[i].TypeResource, -NeedToCraft.FindRef(ResourceSlot[i].TypeResource), NeedToCraft, false);
				}


			}
			
				

		}
		
	}

}

void UCraftComponent::CraftModuleComplite()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StartCraft);
	ABaseCharacter* myChar = Cast<ABaseCharacter>(GetOwner());
	if (myChar)
	{
		bIsCrafting = false;
		myChar->bCrafting = false;
		OnEndCraft.Broadcast();
	}
	
}



bool UCraftComponent::CheckResourceToCraft(TMap<ECraftResource, int32> NeedToCraft)
{
	bool bIsOk = true;
	int8 i = 0;
	while (i < ResourceType.Num() && bIsOk)
	{
		if (ResourceMap.FindRef(ResourceType[i]) >= NeedToCraft.FindRef(ResourceType[i]))
		{
			OnResourceOk.Broadcast(ResourceType[i]);
			bIsOk = true;
		}
		else
		{
			bIsOk = false;
			OnResourceNeedMore.Broadcast(ResourceType[i]);
		}
			

		i++;
	}
	return bIsOk;
}

